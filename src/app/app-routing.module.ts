import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule  } from '@angular/router';

export const routes: Routes = [
 {
    path: '',
    loadChildren : () => import('./pages/home/home.module').then(m => m.HomeModule), // new dynamic import method
 }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes, {
  // preloadingStrategy: PreloadAllModules,  // <- comment this line for activate lazy load
   // useHash: true
});



