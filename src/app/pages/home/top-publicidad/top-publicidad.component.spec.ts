import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopPublicidadComponent } from './top-publicidad.component';

describe('TopPublicidadComponent', () => {
  let component: TopPublicidadComponent;
  let fixture: ComponentFixture<TopPublicidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopPublicidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopPublicidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
