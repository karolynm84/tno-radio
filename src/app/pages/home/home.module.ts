import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeBodyComponent } from './home-body/home-body.component';
import { TopPublicidadComponent } from './top-publicidad/top-publicidad.component';

export const routes = [
  { path: '', component: HomeBodyComponent, pathMatch: 'full'  }
];

@NgModule({
  declarations: [
    HomeBodyComponent,
    TopPublicidadComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class HomeModule { }
